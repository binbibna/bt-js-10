function nguoiDung(
    _tk,
    _name,
    _email,
    _password,
    _datepicker,
    _luongCB,
    _chucvu,
    _gioLam
){
        this.tk= _tk,
        this.name= _name,
        this.email= _email,
        this.password= _password,
        this.date= _datepicker,
        this.luongCB= _luongCB,
        this.chucvu= _chucvu,
        this.gioLam= _gioLam;
        this.tongLuong = function(){
            if(this.chucvu == "Giám Đốc"){
                var x = this.luongCB *3;
                x = x.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
                return x;
            }else if(this.chucvu == "Trưởng Phòng"){
                var x = this.luongCB *2;
                x = x.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
                return x;
            }else if(this.chucvu == "Nhân Viên"){
                var x = this.luongCB*1;
                x = x.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
                return x;
            }
        };
        this.xepLoai = function(){
            if(this.gioLam >= 192){
                return "Xuất Sắc";
            }else if(this.gioLam >= 176){
                return "Giỏi";
            }else if(this.gioLam >= 160){
                return "Khá";
            }else if(this.gioLam < 160){
                return "Trung bình";
            }
        };
}
