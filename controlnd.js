function layThongTin(){
    const _tk = document.getElementById('tknv').value; 
    const _name = document.getElementById("name").value; 
    const _email = document.getElementById("email").value; 
    const _password = document.getElementById("password").value; 
    const _datepicker = document.getElementById("datepicker").value; 
    const _luongCB = document.getElementById("luongCB").value; 
    const _chucvu = document.getElementById("chucvu").value; 
    const _gioLam = document.getElementById("gioLam").value; 


    var nd = new nguoiDung(
      _tk,
      _name,
      _email,
      _password,
      _datepicker,
      _luongCB,
      _chucvu,
      _gioLam
  );


    return nd;

}


function rederDSND(ndArr){
    var contentHTML =[];
    for ( var index = 0; index < ndArr.length; index++){
    var item = ndArr[index];
    var contentTr = 
    `
    <tr>
        <td>${item.tk}</td>
        <td>${item.name}</td>
        <td>${item.email}</td>
        <td>${item.date}</td>
        <td>${item.chucvu}</td>
        <td>${item.tongLuong()}</td>
        <td>${item.xepLoai()}</td>
        <td>
            <button onclick="xoaSinhVien('${item.tk}')" class="btn btn-danger">Xóa</button>

            <button onclick="suaSinhVien('${item.tk}')" class="btn btn-warning">Sửa</button>
        </td>
    </tr>
    `;
      contentHTML += contentTr;
  }
document.getElementById("tableDanhSach").innerHTML=contentHTML;

}



function timKiemViTri(id, arr){
    var viTri = -1;
    for ( var index = 0; index < arr.length; index++){
      var nd = arr[index];
      if(nd.tk == id){
        viTri = index;
        break;
      }  
    }
    return viTri;
  }


  function showThongTinLenForm(nd){
    document.getElementById('tknv').value = nd.tk;
    document.getElementById('name').value = nd.name;
    document.getElementById('email').value = nd.email;
    document.getElementById('password').value = nd.password;
    document.getElementById('chucvu').value = nd.chucvu;
    document.getElementById('gioLam').value = nd.gioLam;
    document.getElementById('luongCB').value = nd.luongCB;
    document.getElementById('datepicker').value = nd.date;
  }