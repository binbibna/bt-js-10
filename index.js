var DSND = "DSND";
var dsnd = [];

var dsndJson = localStorage.getItem("DSND");

if(dsndJson != null){
  var ndArr = JSON.parse(dsndJson);
  dsnd = ndArr.map(function (item){
    return new nguoiDung(
      item.tk, 
      item.name, 
      item.email, 
      item.password, 
      item.date, 
      item.luongCB, 
      item.chucvu,
      item.gioLam
      );
  });
  // convert array chứa object ko có key tinhDTB() thành array chứa object có key tinhDTB()
  rederDSND(dsnd);
}


function themNguoiDung(){
  var nd = layThongTin();
  var isValid = true;

  //validate
  isValid = kiemTraTrung(nd.tk, dsnd) && kiemTraDoDai(nd.tk, "tbTKNV", 4, 8) && kiemTraSo(nd.tk, "tbTKNV");
  isValid = isValid & kiemTraEmail(nd.email);
  isValid = isValid & luongCB(nd.luongCB);
  isValid = isValid & nameTen(nd.name,"tbTen") & chucVu(nd.chucvu);
  isValid = isValid & matkhau(nd.password, "tbMatKhau") & giolam(nd.gioLam);
  if(isValid){
    // nếu isValid true
    dsnd.push(nd);
  // lưu vào localStorage (không bị mất dữ liệu khi load trang)


  //converr array dssv thành json
    var dsndJson = JSON.stringify(dsnd);
  // lưu data json vào localStorage
    localStorage.setItem("DSND", dsndJson);

  // render dssv ra table
    rederDSND(dsnd);
  }
}



function xoaSinhVien(idNd){
  var viTri =timKiemViTri(idNd,dsnd);

  if(viTri != -1){
    dsnd.splice(viTri, 1);
    console.log(xoaSinhVien, dsnd.length);
    rederDSND(dsnd);
  }

  var dsndJson = JSON.stringify(dsnd);
// lưu data json vào localStorage
  localStorage.setItem("DSND", dsndJson);

// render dssv ra table
  rederDSND(dsnd);
}



function suaSinhVien(idNd){
  var viTri = timKiemViTri(idNd, dsnd);
  if(viTri == -1){
    return;
  }
  var nd = dsnd[viTri];
  console.log(nd);
  showThongTinLenForm(nd);

  var dsndJson = JSON.stringify(dsnd);
// lưu data json vào localStorage
  localStorage.setItem("DSND", dsndJson);

// render dssv ra table
  rederDSND(dsnd);
}


function capNhat(){
  // lấy thông tin sau khi user uppdate
  var nd = layThongTin();
  // update dữ liệu mới thay dữ liệu cũ

  var viTri = timKiemViTri(nd.tk, dsnd);
  if(viTri != -1){
    dsnd[viTri] = nd;
    rederDSND(dsnd);
  }


  
  var isValid = true;

  //validate
  isValid = kiemTraDoDai(nd.tk, "tbTKNV", 4, 8) && kiemTraSo(nd.tk, "tbTKNV");
  isValid = isValid & kiemTraEmail(nd.email);
  isValid = isValid & luongCB(nd.luongCB);
  isValid = isValid & nameTen(nd.name,"tbTen") & chucVu(nd.chucvu);
  isValid = isValid & matkhau(nd.password, "tbMatKhau") & giolam(nd.gioLam);
  if(isValid){
    // nếu isValid true
    // dsnd.push(nd);
  // lưu vào localStorage (không bị mất dữ liệu khi load trang)


  //converr array dssv thành json
    var dsndJson = JSON.stringify(dsnd);
  // lưu data json vào localStorage
    localStorage.setItem("DSND", dsndJson);

  // render dssv ra table
    rederDSND(dsnd);
  }
}

function timKiem(){

  var thongTin = document.getElementById("searchName").value;
  thongTin = thongTin.charAt(0).toUpperCase() + thongTin.slice(1);
  var contentHTML =[];
  for ( var index = 0; index < dsnd.length; index++){
  var item = dsnd[index];
  if(item.xepLoai() == thongTin){
    var contentTr = 
  `
  <tr>
      <td>${item.tk}</td>
      <td>${item.name}</td>
      <td>${item.email}</td>
      <td>${item.date}</td>
      <td>${item.chucvu}</td>
      <td>${item.tongLuong()}</td>
      <td>${item.xepLoai()}</td>
      <td>
            <button onclick="xoaSinhVien('${item.tk}')" class="btn btn-danger">Xóa</button>

            <button onclick="suaSinhVien('${item.tk}')" class="btn btn-warning">Sửa</button>
        </td>
  </tr>
  `;
    contentHTML += contentTr;
}
document.getElementById("tableDanhSach").innerHTML=contentHTML;
}
}


