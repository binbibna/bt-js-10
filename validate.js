// true => hợp lệ


function kiemTraTrung(idNd, ndArr){
    var index =ndArr.findIndex(function(item){
        return idNd == item.tk;
    });
    if(index == -1){
        // ko tìm thấy
        document.getElementById("tbTKNV").innerText="";
        return true;
    }else{
        // show thông báo
        document.getElementById("tbTKNV").innerText="Mã đã tồn tại";
        document.getElementById("tbTKNV").style.display="block";
        return false;
    }
}

function kiemTraDoDai(value,idErr,min,max){
    var length = value.length;
    if ( length < min || length > max){
        document.getElementById(idErr).innerText=` Độ dài phải từ ${min} đến ${max} kí tự`;
        document.getElementById(idErr).style.display="block";
        return false;
    }else{
        document.getElementById(idErr).innerText="";
        return true;       
    }
}

function kiemTraSo(value, idErr){
    var reg = /^\d+$/;
    var isNumber = reg.test(value);
    if(isNumber){
        document.getElementById(idErr).innerText="";
        return true;
    }else{
        document.getElementById(idErr).innerText="Nhập số";
        document.getElementById(idErr).style.display="block";
        return false;
    }
}


function kiemTraEmail(value){
    const reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = reg.test(value);
    if(isEmail){
        document.getElementById("tbEmail").innerText="";
        return true;
    }else{
        document.getElementById("tbEmail").innerText="Nhập email";
        document.getElementById("tbEmail").style.display="block";
        return false;
    }
}





function luongCB(value){
    var isLuong = value;
    if(isLuong > 1000000 && isLuong < 20000000){
        document.getElementById("tbLuongCB").innerText="";
        return true;
    } else{
        document.getElementById("tbLuongCB").innerText="Lương ít nhất là 1.000.000 và nhiều nhất là 20.000.000";
        document.getElementById("tbLuongCB").style.display="block";
        return false;
    }
}


function nameTen(value, idErr){
    var isname = value.length;
    if(isname > 0){
        const reg = /^[a-zA-Z]*$/;
        var isTen = reg.test(value);
        if(isTen){
            document.getElementById("tbTen").innerText="";
            return true;
        } else{
            document.getElementById("tbTen").innerText="Chỉ nhập chữ a-z";
            document.getElementById("tbTen").style.display="block";
            return false;
        }
    } else{
        document.getElementById(idErr).innerText="Vui lòng điền đầy đủ";
        document.getElementById(idErr).style.display="block";
        return false;
    }
}


function matkhau(value, idErr){
    var isname = value.length;
    if(isname > 0){
        if ( isname > 10 || isname < 6){
            document.getElementById("tbMatKhau").innerText=` Độ dài phải từ 6 đến 10 kí tự`;
            document.getElementById("tbMatKhau").style.display="block";
            return false;
        }else{
            
            const reg = /(?=[^A-Z]*[A-Z])(?=[^!@#\$%]*[!@#\$%])/;
            var ismk = reg.test(value);
            if(ismk){
                document.getElementById("tbMatKhau").innerText="";
                return true;
            } else{
                document.getElementById("tbMatKhau").innerText="vui lòng chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt";
                document.getElementById("tbMatKhau").style.display="block";
                return false
            }
        }
    } else{
        document.getElementById(idErr).innerText="Vui lòng điền đầy đủ";
        document.getElementById(idErr).style.display="block";
        return true;
    }
}


function giolam(value){
    var isGio = value ;
    if(isGio > 200 || isGio < 80){
        document.getElementById("tbGiolam").innerText="Giờ làm từ 80h đến 20h";
        document.getElementById("tbGiolam").style.display="block";
        return false;
    }else{
        document.getElementById("tbGiolam").innerText="";
        return true;
    }
}


function chucVu(value){
    var isChucvu = value;
    if ( isChucvu == "Chọn chức vụ"){
        document.getElementById("tbChucVu").innerText="Vui lòng chọn";
        document.getElementById("tbChucVu").style.display="block";
        return false;
    } else{
        document.getElementById("tbChucVu").innerText="";
        return true;
    }
}
//// tới khúc giờ làm ròi nha 